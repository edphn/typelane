import React from 'react';
import PropTypes from 'prop-types';
import CustomPropTypes from '../../shared/custom-prop-types';
import { Button, Code, ListItem, UnorderedList } from 'evergreen-ui';

const MisspellingsList = ({ onSuggestionPick, words }) => (
    <UnorderedList>
        {words.filter(word => word.misspelling).map(({ misspelling }) => (
            <ListItem key={misspelling.start}>
                <Code>{misspelling.word}</Code>&nbsp;
                {misspelling.suggestions.length
                    ? (
                        <>
                            can be replaced with:&nbsp;
                            {misspelling.suggestions.map((suggestion, index) => (
                                <span key={index} onClick={() => onSuggestionPick(misspelling.start, suggestion)}>
                                    <Button height={20}>{suggestion}</Button>&nbsp;
                                </span>
                            ))}
                        </>
                    )
                    : null
                }
            </ListItem>
        ))}
    </UnorderedList>
);

MisspellingsList.propTypes = {
    onSuggestionPick: PropTypes.func.isRequired,
    words: PropTypes.arrayOf(PropTypes.shape({
        content: PropTypes.string.isRequired,
        misspelling: CustomPropTypes.misspelling
    }))
};

export default MisspellingsList;
