import { cloneDeep } from 'lodash';

/**
 * Return bounded function that designates
 * misspellings for each words.
 *
 * @param {Array} misspellings
 * @returns {Function}
 */
export const assignMisspellings = misspellings => word => {
    let misspelling = null;

    if (misspellings[0]) {
        misspelling = (word === misspellings[0].word) ? misspellings.shift() : null;
    }

    return {
        content: word,
        misspelling
    };
};

/**
 * Apply first suggestion to word if exists.
 *
 * @param {string} word
 * @returns {Array}
 */
export const applyFirstSuggestion = word => word.misspelling
    ? ({
        content: word.misspelling.suggestions[0] || word.content,
        misspelling: null
    })
    : word;

/**
 * Extract words out of text.
 *
 * @param {string }text
 * @param {Array} misspellings
 * @returns {Array}
 */
export const extractWords = (text, misspellings) => text.trim()
    .split(/\s+/)
    .map(assignMisspellings(cloneDeep(misspellings)));

/**
 * Concatenate words array intro straight text.
 * @param words
 * @returns {string}
 */
export const concatenateWords = words => words.map(({ content }) => content).join(' ');
