import React, { useState } from 'react';
import PropTypes from 'prop-types';
import CustomPropTypes from '../../shared/custom-prop-types';
import { Alert, Button, Pane, Text, Textarea, Tooltip } from 'evergreen-ui';
import MisspellingsList from '../MisspellingsList';
import { applyFirstSuggestion, concatenateWords, extractWords } from './support';

const SpellcheckingSummary = ({ onDownload, onStartOver, summary: { originalText, misspellings } }) => {
    const [words, setWords] = useState(extractWords(originalText, misspellings));
    const anyMisspellingsFound = words.filter(word => word.misspelling).length;

    const handlePickSuggestion = (startPosition, suggestion) => {
        const newWords = words.map(word => word.misspelling?.start === startPosition
            ? ({
                content: suggestion,
                misspelling: null
            })
            : word);

        setWords(newWords);
    };

    const handleAutomaticFix = (words) => {
        setWords(words.map(applyFirstSuggestion));
    };

    const handleDownload = words => {
        onDownload(words.map(({ content }) => content).join(' '));
    };

    return (
        <>
            <Alert
                intent={anyMisspellingsFound ? 'warning' : 'success'}
                title={anyMisspellingsFound ? 'Your text have some misspellings.' : 'Your text does not have any misspellings. Perfect!' }
            />
            {anyMisspellingsFound ? (
                <Pane marginTop={20}>
                    <Text marginBottom={10}>
                        Following words are misspelled.
                        Click on the suggestion to apply or&nbsp;
                        <Tooltip content='Will apply first suggestion if exists.'>
                            <Button
                                appearance='primary'
                                height={20}
                                intent='success'
                                onClick={() => handleAutomaticFix(words)}
                            >
                                fix automatically
                            </Button>
                        </Tooltip>
                    </Text>
                    <MisspellingsList words={words} onSuggestionPick={handlePickSuggestion} />
                </Pane>
            ) : null}
            <Textarea value={concatenateWords(words)} disabled marginTop={20}></Textarea>

            <Pane marginTop={20}>
                <Button height={40} onClick={onStartOver}>Start over</Button>
                <Button height={40} marginLeft={5} onClick={() => handleDownload(words)}>Download</Button>
            </Pane>
        </>
    );
};

SpellcheckingSummary.propTypes = {
    onDownload: PropTypes.func.isRequired,
    onStartOver: PropTypes.func.isRequired,
    summary: PropTypes.shape({
        originalText: PropTypes.string.isRequired,
        misspellings: PropTypes.arrayOf(CustomPropTypes.misspelling).isRequired
    })
};

export default SpellcheckingSummary;
