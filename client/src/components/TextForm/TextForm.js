import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Button, Text, Textarea } from "evergreen-ui";

const TextForm = ({ onSubmit }) => {
    const [text, setText] = useState('Hello teh pepole from aruond teh world!');

    const handleSubmitClick = () => {
        onSubmit(text);
    };

    return (
        <>
            <Text>Provide text you want to run over spellchecking process.</Text>
            <Textarea height={200} marginY={10} onChange={e => setText(e.target.value)} value={text}></Textarea>
            <Button appearance='primary' disabled={!text} height={40} onClick={handleSubmitClick}>Submit!</Button>
            <Button height={40} marginLeft={5} onClick={() => {setText('')}}>Clear</Button>
        </>
    );
};

TextForm.propTypes = {
    onSubmit: PropTypes.func.isRequired,
};

export default TextForm;
