import React, { useState } from 'react';
import downloadjs from 'downloadjs';
import { performSpellchecking } from "../../shared/api";
import { Heading, Pane } from 'evergreen-ui';
import TextForm from '../TextForm';
import SpellcheckingSummary from '../SpellcheckingSummary';

const App = () => {
    const [spellcheckingSummary, setSpellcheckingSummary] = useState(null);

    const handleCheck = async text => {
        setSpellcheckingSummary(await performSpellchecking(text));
    };

    const handleDownload = text => {
        downloadjs(text, 'fixed.txt', 'text/plain');
    };

    return (
        <Pane alignItems='center' backgroundColor='white' elevation={0} margin={24} padding={30}>
            <Heading is='h1' marginBottom={30} size={700} textAlign='center'>Spellcheck!</Heading>
            {!spellcheckingSummary ? (
                <TextForm onSubmit={handleCheck} />
            ) : (
                <SpellcheckingSummary
                    onDownload={handleDownload}
                    onStartOver={() => setSpellcheckingSummary(null)}
                    summary={spellcheckingSummary}
                />
            )}
        </Pane>
    );
};

export default App;
