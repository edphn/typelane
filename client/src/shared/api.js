/**
 * Perform spellchecking through the API.
 *
 * @param {string} text
 * @returns {Promise<any>}
 */
export const performSpellchecking = async text => {
    const response = await fetch('/api/spellchecking/check', {
        headers: {
            'Content-Type': 'application/json'
        },
        method: 'POST',
        body: JSON.stringify({ text })
    });

    return await response.json();
};
