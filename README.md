# Typelane Assessment Task

## How to run the API
1. In your terminal, navigate to the `api` directory.
2. Run `npm install` to install all dependencies.
3. Run `npm run start` to start the app.

## How to run Client application
1. In separate terminal tab, navigate to the `client` directory.
2. Run `npm install` to install all dependencies.
3. Run `yarn start`
4. Within our browser navigate to `localhost:3000`
