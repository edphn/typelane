const express = require('express');
const bodyParser = require('body-parser');
const { findMisspellings } = require('./src/spellchecking');

const app = express();
const port = 5000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.post('/api/spellchecking/check', (req, res) => {
    res.send(({
        originalText: req.body.text,
        misspellings: findMisspellings(req.body.text)
    }));
});

app.listen(port, () => console.log(`Listening on port ${port}`));
