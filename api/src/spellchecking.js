const SpellChecker = require('spellchecker');

SpellChecker.setDictionary('en');

/**
 * Return curried function which extracts
 * words by position from given text.
 *
 * @param {string} text
 * @returns {Function}
 */
const extractMisspelledWords = text => ({ start, end }) => ({ start, end, word: text.slice(start, end)});

/**
 * Find correction suggestions for misspellings.
 *
 * @param {Object} misspelling
 * @returns {Object}
 */
const findCorrections = misspelling => ({ ...misspelling, suggestions: SpellChecker.getCorrectionsForMisspelling(misspelling.word)})

/**
 * Find misspelling within passed text.
 *
 * @param {String} text
 * @returns {Array}
 */
const findMisspellings = text => SpellChecker
    .checkSpelling(text)
    .map(extractMisspelledWords(text))
    .map(findCorrections);

module.exports = {
    findMisspellings
};
